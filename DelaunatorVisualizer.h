#pragma once

#include <vector>
#include "Common/Types.h"
#include "Visible/Visible.h"
#include "Messenger/Listener.h"
#include "Delaunator CPP/include/delaunator.hpp"

class CDelaunatorVisualizer : public CVisible, public CListener
{
public:
			CDelaunatorVisualizer() = delete;
			CDelaunatorVisualizer( const delaunator::Delaunator& krDelaunator );
			~CDelaunatorVisualizer() override;
	void	Draw() const override;
	void	Push( CMessage& ) override;

private:
	const delaunator::Delaunator&	m_krDelaunator;
};
