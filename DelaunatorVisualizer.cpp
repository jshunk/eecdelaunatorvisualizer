#include "DelaunatorVisualizer.h"
#include "Messenger/Messenger.h"
#include "Window/Window.h"
#include "windows.h"
#include <gl\gl.h>

CDelaunatorVisualizer::CDelaunatorVisualizer(const delaunator::Delaunator& krDelaunator)
	: m_krDelaunator( krDelaunator )
{
	assert( CWindow::GetDefaultWindow() );
	CMessenger::GlobalListen( *this, CWindow::GetDefaultWindow()->GetDraw2DMessage() );
}

CDelaunatorVisualizer::~CDelaunatorVisualizer()
{
	assert( CWindow::GetDefaultWindow() );
	CMessenger::GlobalStopListening( *this, CWindow::GetDefaultWindow()->GetDraw2DMessage() );
}

void CDelaunatorVisualizer::Draw() const
{
	f32 fWindowHalfWidth = CWindow::GetDefaultWindow()->GetVirtualWidth() / 2;
	f32 fWindowHalfHeight = CWindow::GetDefaultWindow()->GetVirtualHeight() / 2;

	glColor3f( 1.f, 1.f, 1.f );
	glBegin( GL_TRIANGLES );
		for( std::size_t i = 0; i < m_krDelaunator.triangles.size(); i += 3 )
		{
			f32 fX1 = NUtilities::ConvertCoordinateNoBounds( (f32)m_krDelaunator.coords[2 * m_krDelaunator.triangles[i]], -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
			f32 fX2 = NUtilities::ConvertCoordinateNoBounds( (f32)m_krDelaunator.coords[2 * m_krDelaunator.triangles[i + 1]], -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
			f32 fX3 = NUtilities::ConvertCoordinateNoBounds( (f32)m_krDelaunator.coords[2 * m_krDelaunator.triangles[i + 2]], -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
			f32 fY1 = NUtilities::ConvertCoordinateNoBounds( (f32)m_krDelaunator.coords[2 * m_krDelaunator.triangles[i] + 1], -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
			f32 fY2 = NUtilities::ConvertCoordinateNoBounds( (f32)m_krDelaunator.coords[2 * m_krDelaunator.triangles[i + 1] + 1], -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
			f32 fY3 = NUtilities::ConvertCoordinateNoBounds( (f32)m_krDelaunator.coords[2 * m_krDelaunator.triangles[i + 2] + 1], -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
            glVertex2f( fX1, fY1 );
            glVertex2f( fX2, fY2 );
            glVertex2f( fX3, fY3 );
		}
	glEnd();
	glColor3f( 1.f, 0.f, 1.f );
	glBegin( GL_LINES );
		for( std::size_t i = 0; i < m_krDelaunator.triangles.size(); i += 3 )
		{
			f32 fX1 = NUtilities::ConvertCoordinateNoBounds( (f32)m_krDelaunator.coords[2 * m_krDelaunator.triangles[i]], -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
			f32 fX2 = NUtilities::ConvertCoordinateNoBounds( (f32)m_krDelaunator.coords[2 * m_krDelaunator.triangles[i + 1]], -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
			f32 fX3 = NUtilities::ConvertCoordinateNoBounds( (f32)m_krDelaunator.coords[2 * m_krDelaunator.triangles[i + 2]], -fWindowHalfWidth, fWindowHalfWidth, -1.f, 1.f );
			f32 fY1 = NUtilities::ConvertCoordinateNoBounds( (f32)m_krDelaunator.coords[2 * m_krDelaunator.triangles[i] + 1], -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
			f32 fY2 = NUtilities::ConvertCoordinateNoBounds( (f32)m_krDelaunator.coords[2 * m_krDelaunator.triangles[i + 1] + 1], -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
			f32 fY3 = NUtilities::ConvertCoordinateNoBounds( (f32)m_krDelaunator.coords[2 * m_krDelaunator.triangles[i + 2] + 1], -fWindowHalfHeight, fWindowHalfHeight, -1.f, 1.f );
            glVertex2f( fX1, fY1 );
            glVertex2f( fX2, fY2 );
            glVertex2f( fX2, fY2 );
            glVertex2f( fX3, fY3 );
            glVertex2f( fX3, fY3 );
            glVertex2f( fX1, fY1 );
		}
	glEnd();
}

void CDelaunatorVisualizer::Push( CMessage& )
{
	Draw();
}
